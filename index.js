/*
	1. Create a function named getUserInfo which is able to return an object. 

		The object returned should have the following properties:
		
		- key - data type

		- name - String
		- age -  Number
		- address - String
		- isMarried - Boolean
		- petName - String

		Note: Property names given is required and should not be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.

*/

	function getUserInfo() {
		let UserInfo = {
			name: "Marco Hipolito",
			age: 22,
			address: "Calamba City, Laguna",
			isMarried: false,
			petName: "Wiggles"
		}

		return UserInfo;
	}

	let myUserInfo = getUserInfo();
	console.log(myUserInfo);

/*
	2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
		
		- Note: the array returned should have at least 5 elements as strings.
			    function name given is required and cannot be changed.


		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
	
*/
	function getArtistsArray() {
		let ArtistArray = ["The 1975", "Cigarettes After S", "The Midnight", "Night Traveler", "The Paper Kites"];

		return ArtistArray;
	};

	let ArtistArray = getArtistsArray();
	console.log(ArtistArray)



/*
	3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/
	function getSongsArray() {
		let SongsArray = ["Robbers", "About You", "Apocalypse", "Memories", "1984"];

		return SongsArray;
	};
	let SongsArray = getSongsArray();
	console.log(SongsArray);



/*
	4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/
	function getMoviesArray() {
		let MoviesArray = ["Meet Joe Black", "Be With you", "Film Z", "On Your Wedding Day", "Fault in Our Stars"];

		return MoviesArray;
	};
	let MoviesArray = getMoviesArray();
	console.log(MoviesArray);



/*
	5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

			- Note: the array returned should have numbers only.
			        function name given is required and cannot be changed.

			To check, create a variable to save the value returned by the function.
			Then log the variable in the console.

			Note: This is optional.
			
*/
	function getPrimeNumberArray() {
		let PrimeNumberArray = [2, 3, 5, 7, 11];

		return PrimeNumberArray;
	};
	let PrimeNumberArray = getPrimeNumberArray();
	console.log(PrimeNumberArray);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
		getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
		getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
		getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
		getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

	}
} catch(err){


}